#import the necessary packages
from imutils.perspective import four_point_transform
from imutils import contours
import imutils
import numpy as np
import cv2
import argparse

# define the dictionary of digit segments so we can identify
# each digit on the thermostat
DIGITS_LOOKUP = {
	(1, 1, 1, 0, 1, 1, 1): 0,
	(0, 0, 1, 0, 0, 1, 0): 1,
	(0, 1, 0, 0, 1, 0, 0): 1,
	(1, 0, 1, 1, 1, 1, 0): 2,
	(1, 0, 1, 1, 1, 0, 1): 2,
	(1, 0, 1, 1, 0, 1, 1): 3,
	(0, 1, 1, 1, 0, 1, 0): 4,
	(1, 1, 0, 1, 0, 1, 1): 5,
	(1, 1, 0, 1, 1, 1, 1): 6,
	(1, 0, 1, 0, 0, 1, 0): 7,
	(1, 1, 1, 1, 1, 1, 1): 8,
	(1, 1, 1, 1, 0, 1, 1): 9
}

#-------------------------------------------------------------------------
def preprocess(image):
  # pre-process the image by resizing it, converting it to
  # graycale, blurring it, and computing an edge map
  image = imutils.resize(image, height=500)
  gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  blurred = cv2.GaussianBlur(gray, (5, 5), 0)
  edged = cv2.Canny(blurred, 50, 200, 255)

  return [image, gray, blurred, edged]


#-------------------------------------------------------------------------
def find_contours(edged):
  cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
  	cv2.CHAIN_APPROX_SIMPLE)
  cnts = imutils.grab_contours(cnts)
  cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
  displayCnt = None
  # loop over the contours
  for c in cnts:
  	# approximate the contour
  	peri = cv2.arcLength(c, True)
  	approx = cv2.approxPolyDP(c, 0.02 * peri, True)
  	# if the contour has four vertices, then we have found
  	# the thermostat display
  	if len(approx) == 4:
  		displayCnt = approx
  		break

  return displayCnt

#-------------------------------------------------------------------------
def threshold(warped):
  thresh = cv2.threshold(warped, 0, 500,
  	cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
  cv2.imwrite( saveName+"_step3a_thresh.jpg", thresh);
  kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 8))
  #thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
  thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
  cv2.imwrite( saveName+"_step3b_morphed.jpg", thresh);

  return thresh

#-------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser()
  parser.add_argument("-f", "--file", default = 'image', type=str, nargs='+',
                     help="name of input image file")
  parser.add_argument("-d", "--debug", default = 0, type=int, nargs='+',
                     help="do debugging?")
  args = parser.parse_args()
  filename = args.file[0]
  saveName = filename.replace('.jpg','').replace('.JPG','').split('/')[1]
  print('running file ', saveName)

  # load the example image
  image = cv2.imread(filename)
  cv2.imwrite( saveName+"_step0.jpg", image );
  #cv2.imshow("Input", image)
  #cv2.waitKey(0)
  image, gray, blurred, edged = preprocess(image)
  cv2.imwrite( saveName+"_step1_gray.jpg", gray );
  cv2.imwrite( saveName+"_step1_blurred.jpg", blurred );
  cv2.imwrite( saveName+"_step1_edged.jpg", edged );
   
  # find contours in the edge map, then sort them by their
  # size in descending order
  displayCnt = find_contours(edged) 
  #displayCnt = find_contours(gray) 
  
  # extract the thermostat display, apply a perspective transform
  # to it
  warped = four_point_transform(gray, displayCnt.reshape(4, 2))
  #output = four_point_transform(image, displayCnt.reshape(4, 2))
  cv2.imwrite( saveName+"_step2_warped.jpg", warped);
  
  # threshold the warped image, then apply a series of morphological
  # operations to cleanup the thresholded image
  thresh = threshold(warped)
  
  # find contours in the thresholded image, then initialize the
  # digit contours lists
  cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
  	cv2.CHAIN_APPROX_SIMPLE)
  cnts = imutils.grab_contours(cnts)
  digitCnts = []
  # loop over the digit area candidates
  for c in cnts:
    # compute the bounding box of the contour
    (x, y, w, h) = cv2.boundingRect(c)
    # if the contour is sufficiently large, it must be a digit. Save contour
    print('digit area candidate: ', x, y, w, h)
    #orig: if w >= 15 and (h >= 30 and h <= 40):
    if w >= 10 and (h >= 20 and h <= 100):
        print('big enough, append!')
        digitCnts.append(c)
  
  # sort the contours from left-to-right, then initialize the
  # actual digits themselves
  digitCnts = contours.sort_contours(digitCnts,
  	method="left-to-right")[0]
  digits = []
 
  #----------------------------------------
  #--------- ROIs ------------------------ 
  # loop over each of the digits
  print('Looping over ', len(digitCnts), ' digit ROIs')
  jj = 0
  for c in digitCnts:
    # extract the digit ROI
    (x, y, w, h) = cv2.boundingRect(c)
    print('digit ROI: ', x, y, w, h)
    roi = thresh[y:y + h, x:x + w]
    cv2.imwrite( saveName+"_step4_roi"+str(jj)+".jpg", thresh[y:y + h, x:x + w]);
    jj += 1
    # compute the width and height of each of the 7 segments
    # we are going to examine
    (roiH, roiW) = roi.shape
    (dW, dH) = (int(roiW * 0.25), int(roiH * 0.15))
    dHC = int(roiH * 0.05)
    # define the set of 7 segments
    segments = [
    	((0, 0), (w, dH)),	# top
    	((0, 0), (dW, h // 2)),	# top-left
    	((w - dW, 0), (w, h // 2)),	# top-right
    	((0, (h // 2) - dHC) , (w, (h // 2) + dHC)), # center
    	((0, h // 2), (dW, h)),	# bottom-left
    	((w - dW, h // 2), (w, h)),	# bottom-right
    	((0, h - dH), (w, h))	# bottom
    ]
    on = [0] * len(segments)
    
    # loop over the segments
    for (i, ((xA, yA), (xB, yB))) in enumerate(segments):
    	# extract the segment ROI, count the total number of
    	# thresholded pixels in the segment, and then compute
    	# the area of the segment
    	segROI = roi[yA:yB, xA:xB]
    	total = cv2.countNonZero(segROI)
    	area = (xB - xA) * (yB - yA)
    	# if the total number of non-zero pixels is greater than 50% of the area, mark the segment as "on"
    	if total / float(area) > 0.4:
    		on[i]= 1
    # lookup the digit and draw it on the image
    print('tuple of segments: ', tuple(on))
    try: 
      digit = DIGITS_LOOKUP[tuple(on)]
      digits.append(digit)
    except: print('Did not find real number for this code: ', tuple(on))
    #cv2.rectangle(output, (x, y), (x + w, y + h), (0, 255, 0), 1)
    #cv2.putText(output, str(digit), (x - 10, y - 10),
    #	cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
    
  
  
  # display the digits
  #print(u"{}{}.{} \u00b0".format(*digits))
  print("Final digits: ")
  for digit in digits:
    print(digit)
  #cv2.imshow("Input", image)
  #cv2.imshow("Output", output)
  #cv2.waitKey(0)
